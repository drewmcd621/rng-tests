
package rng;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */


public class RNG {
    enum RNGs
{
    JavaRandom,CustomLCG,RANDU    
}

    /**
     * Arg[0] = type of RNG (0= Java, 1=CustomLCG, 2=RANDU)
     * All others:
     * KS = K-S Test
     * CS = Chi Sq Test
     * WW = W-W Runs Test
     * AC = Autocorrelations Test
     */
    //Defaults
    private static String fname = "Results.csv";
    private static int RNGtype = 0;
    private static boolean doKS = true;
    private static boolean doCS = true;
    private static boolean doWW = true;
    private static boolean doAC = true;
    
    public static void main(String[] args) throws FileNotFoundException 
    {
        List<String> arg = Arrays.asList(args);
        RNGtype = Integer.parseInt(args[0]);
        doKS = arg.contains("KS");      
        doCS = arg.contains("CS");   
        doWW = arg.contains("WW");   
        doAC = arg.contains("AC");   
        fname = RNGs.values()[RNGtype].toString() +" Results.csv";
        
        int seed = 1234567890;
        PrintWriter writer;
        writer = new PrintWriter(fname);   
        ArrayList<RandGenNum> rns = genRandNums(RNGs.values()[RNGtype],10000,seed);
        writer.println("Testing Random Generator: ," + RNGs.values()[RNGtype].toString());
        writer.println();
        writer.println();
        if(doKS) KSTest(rns,writer);
        if(doCS) ChiSqTest(rns,writer);
        if(doWW) RunsTest(rns,writer);
        if(doAC) AutoCorrTest(rns,writer);
        printRandom(rns,writer,seed);
        writer.close();
    }
    public static void printRandom(ArrayList<RandGenNum> rnSet, PrintWriter out, int seed)
    {
        out.println("Seed=," + Integer.toString(seed) );
        out.println("Xi,Ri");
        for(RandGenNum r : rnSet)
        {
            out.println(Integer.toString(r.getIndex()) + "," + Double.toString(r.getValue()));
        }
    }    
    private static void KSTest(ArrayList<RandGenNum> rnSet, PrintWriter out)
    {
        KStest test = new KStest();
        double r = test.run(rnSet);
        out.println("K-S test for randomness");
        out.println("D =," + Double.toString(r));
        out.println("alpha,P/F");
        for(KStest.sigLevel s : KStest.sigLevel.values())
        {
            if(test.getResult(s))
            {
                out.println(s.toString().replace("s", "") + "%,Pass");
            }
            else
            {
                out.println(s.toString().replace("s", "") + "%,Fail");
            }
         }
        out.println();
        
    }
     private static void ChiSqTest(ArrayList<RandGenNum> rnSet, PrintWriter out)
    {
        ChiSqTest test = new ChiSqTest();
        double r = test.run(rnSet);
        out.println("Chi-Squared test for randomness");
        out.println("Chi =," + Double.toString(r));
        out.println("alpha,P/F");
        for(ChiSqTest.sigLevel s : ChiSqTest.sigLevel.values())
        {
            if(test.getResult(s))
            {
                out.println(s.toString().replace("s", "") + "%,Pass");
            }
            else
            {
                out.println(s.toString().replace("s", "") + "%,Fail");
            }
         }
        out.println();
        
    }
    private static void RunsTest(ArrayList<RandGenNum> rnSet, PrintWriter out)
    {
        RunsTest test = new RunsTest();
        double r = test.run(rnSet);
        out.println("W-W runs test for independence");
        out.println("Z =," + Double.toString(r));
        out.println("alpha,P/F");
        for(RunsTest.sigLevel s : RunsTest.sigLevel.values())
        {
            if(test.getResult(s))
            {
                out.println(s.toString().replace("s", "") + "%,Pass");
            }
            else
            {
                out.println(s.toString().replace("s", "") + "%,Fail");
            }
         }
        out.println(); 
    }
     private static void AutoCorrTest(ArrayList<RandGenNum> rnSet, PrintWriter out)
    {
        AutoCorrTest test = new AutoCorrTest();
        out.println("Autocorrelation tests for independence");
        
        int[] ltests = {2,3,5,10,25,50};
        
        String[] strs = {"L","Z","80%","90%","95%"};
        for(int l : ltests)
        {
        double r = test.run(rnSet, l);
        strs[0] += "," + Integer.toString(l);
        strs[1] += "," + Double.toString(r);
        int t = 2;
        for(AutoCorrTest.sigLevel s : AutoCorrTest.sigLevel.values())
        {
            if(test.getResult(s))
            {
               strs[t] += ",Pass";
            }
            else
            {
                strs[t] +=  ",Fail";
            }
            t++;
         }
        }
        
        for(String str : strs)
        {
            out.println(str);
        }
        out.println(); 
    }
    
    
    
    private static ArrayList<RandGenNum> genRandNums(RNGs type, int number, int seed)
    {
        if(type == RNGs.JavaRandom)
        {

            return genJavaRandom(number, seed);
        }
        else if(type == RNGs.CustomLCG)
        {

            return genCustomLCG(number, seed);
        }
        else if(type == RNGs.RANDU)
        {

            return genRANDU(number, seed);
        }
        
        return null;
    }
    private static ArrayList<RandGenNum> genJavaRandom(int number, int seed)
    {
        Random r = new Random(seed);
        ArrayList<RandGenNum> RGN = new ArrayList<RandGenNum>();
        for(int i = 0; i < number; i++)
        {
            double rand = r.nextDouble();
            RandGenNum rn = new RandGenNum(i+1,rand);
            RGN.add(rn);
        }
        return RGN;
        
    }
    private static ArrayList<RandGenNum> genCustomLCG(int number, int seed)
    {
        CustomLCG r = new CustomLCG(seed);
        ArrayList<RandGenNum> RGN = new ArrayList<>();
        for(int i = 0; i < number; i++)
        {
            double rand = r.next();
            RandGenNum rn = new RandGenNum(i+1,rand);
            RGN.add(rn);
        }
        return RGN;
        
    }
    private static ArrayList<RandGenNum> genRANDU(int number, int seed)
    {
        RANDU r = new RANDU(seed);
        ArrayList<RandGenNum> RGN = new ArrayList<>();
        for(int i = 0; i < number; i++)
        {
            double rand = r.next();
            RandGenNum rn = new RandGenNum(i+1,rand);
            RGN.add(rn);
        }
        return RGN;
        
    }

}
