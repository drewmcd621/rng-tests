
package rng;

import java.util.ArrayList;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class RunsTest 
{
    enum sigLevel
    {
        s80,s90,s95
    }
    public RunsTest(){}
    
    private ArrayList<RandGenNum> rns;
    private int N;
    private double Z;
    public double run(ArrayList<RandGenNum> randomNumbers)
    {
        rns = (ArrayList<RandGenNum>)randomNumbers.clone();
        N = rns.size();
        double sum = 0;
        for(RandGenNum n : rns )
        {
            sum += n.getValue();
        }
        
        double mean = sum / (double)N;
        
        int plus = 0;
        int minus = 0;
        int sign = 0;
        int runs = 0;
        
        for(RandGenNum n : rns )
        {
            if(n.getValue() >= mean)
            {
                plus++;
                
                if(sign != 1)
                {
                    runs ++;
                    sign = 1;
                }
            }
            else
            {
                minus++;
                if(sign != -1)
                {
                    runs ++;
                    sign = -1;
                }           

            }
        }
        double eRuns = ((2*plus*minus)/(double)N) +1;
        double var = ((eRuns-1)*(eRuns-2))/(double)(N-1);
        Z = (runs - eRuns)/Math.sqrt(var);
        return Z;
    }
    public boolean getResult(sigLevel significance)
    {
        // 80% = 0.2/2 = 0.1 = Z(1.28) or Z(-1.28)
        // 90% = 0.1/2 = 0.05 = Z(1.65) or Z(-1.65)
        // 95% = 0.05/2 = 0.025 = Z(1.96) or Z(-1.96)
         double sigVal[] = {1.28, 1.65, 1.96};
         double Zap = sigVal[significance.ordinal()];
         double Zan = -1*sigVal[significance.ordinal()];
         
         if(Z > Zan && Z < Zap) return true;
        return false;
    }

}
