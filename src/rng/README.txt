Drew McDermott
10/1/2013
-----------------

Usage:

RNG type [KS] [CS] [WW] [AC]

type - Type of random number generator
0 = Java's RNG
1 = Custom LNG
2 = RANDU

KS = do K-S Test
CS = do Chi Sqaured test
WW = do W-W Runs Test
AC = do Autocorrelation tests

Output:

Outputs to a file named "[RNG Type] Results.csv" which includes the results of each test
as well as the random numbers that were generated.