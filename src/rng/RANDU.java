
package rng;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class RANDU 
{
    private double a = 65539;
    private double c = 0;
    private double m = Math.pow(2, 31);
    private double x0;
    private double xi;
    
    public RANDU(int seed)
    {
        x0 = seed;
        xi = seed;
    }
    
    public double next()
    {
        xi = (a*xi + c) % m;
        return xi / m;
    }

}
