
package rng;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class ChiSqTest 
{
      enum sigLevel
    {
        s80,s90,s95
    }
    public ChiSqTest(){}

    private int N;
    private int Ev;
    private ArrayList<RandGenNum> rns;
    private int[] Ov = new int[10];
    private double chi;
    public double run(ArrayList<RandGenNum> randomNumbers)
    {
        rns = (ArrayList<RandGenNum>)randomNumbers.clone();
        N = rns.size();
        Ev = (int)(N / 10); //Expected value
        
        //Sort rns
        Collections.sort(rns);
        
        int i = 0;
        for(int s = 0; s < 10; s ++ )
        {
            
            Ov[s] = 0;
            double th = (s+1) / 10.0;
            while(rns.get(i).getValue() < th)
            {
                Ov[s] ++;
                i ++;
                if(i >= N) break;
            }
        }
        
        double sum = 0;
        for(int j = 0; j < 10; j++)
        {
            sum += Math.pow(Ov[j] - Ev, 2) / Ev;
        }
        chi = sum;
        
        return chi;
    }
    public boolean getResult(sigLevel significance)
    {
        double sigVal[] = {111.7,118.5,124.3}; //From http://www.unc.edu/~farkouh/usefull/chi.html (N=100)
        double crit = sigVal[significance.ordinal()];
        return (chi < crit);
    }
    
}
