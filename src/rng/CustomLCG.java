
package rng;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class CustomLCG 
{
    private double a = 101427;
    private double c = 321;
    private double m = Math.pow(2, 16);
    private double x0;
    private double xi;
    
    public CustomLCG(int seed)
    {
        x0 = seed;
        xi = seed;
    }
    
    public double next()
    {
        xi = (a*xi + c) % m;
        return xi / m;
    }

}
