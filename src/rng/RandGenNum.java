
package rng;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class RandGenNum implements Comparable<RandGenNum>
{
    private int xi;
    private double val;
    public RandGenNum(int index, double value)
    {
        xi = index;
        val = value;
    } 
    public int compareTo(RandGenNum that)
    {
        if(this.val > that.val)
        {
            return 1;
        }
        else if(this.val == that.val)
        {
            return 0;
        }
        else if(this.val < that.val)
        {
            return -1;
        }
        return 0;
    }
    public int getIndex()
    {
        return xi;
    }
    public double getValue()
    {
        return val;
    }
    
    @Override
    public String toString()
    {
        return Double.toString(val);
    }
    
    
}
