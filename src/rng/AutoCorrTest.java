
package rng;

import java.util.ArrayList;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class AutoCorrTest {
        enum sigLevel
    {
        s80,s90,s95
    }
    public AutoCorrTest()
    {
        
    }
    private int start = 1; //This is the i value
    private int N;
    private ArrayList<RandGenNum> rns;
    private int M;
    private double P;
    private double stDev;
    private double Z;
    public double run(ArrayList<RandGenNum> randomNumbers, int lag)
    {
        rns = (ArrayList<RandGenNum>)randomNumbers.clone();
        N = rns.size();
        M = (int)((double)(N - (lag + start))/(double)lag);
        
        //Get P
        double sum = 0;
        double r = rns.get(start - 1).getValue();
        double rn = 0;
        for(int i = 1; i < M; i++)
        {
            rn = rns.get(start + i*lag).getValue();
            sum += r*rn;
            r = rn;
        }
        sum *= (1.0/(double)(M + 1));
        sum -= 0.25;
        P = sum;
        
        stDev = Math.sqrt(13*M + 7)/(double)(12*(M+1));        
        
        Z = P/stDev;
        return Z;
        
    }
    public boolean getResult(sigLevel significance)
    {
        // 80% = 0.2/2 = 0.1 = Z(1.28) or Z(-1.28)
        // 90% = 0.1/2 = 0.05 = Z(1.65) or Z(-1.65)
        // 95% = 0.05/2 = 0.025 = Z(1.96) or Z(-1.96)
         double sigVal[] = {1.28, 1.65, 1.96};
         double Zap = sigVal[significance.ordinal()];
         double Zan = -1*sigVal[significance.ordinal()];
         
         if(Z > Zan && Z < Zap) return true;
        return false;
    }
}
