
package rng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class KStest 
{
    enum sigLevel
    {
        s80,s90,s95
    }
    private List<RandGenNum> rns;
    private double Dtest;
    private int N;
    public KStest()
    {}
    public double run(ArrayList<RandGenNum> randomNumbers)
    {
        rns = (List<RandGenNum>)randomNumbers.clone();
        if(rns.size() > 100)
        {
            rns = rns.subList(0, 100); //Get first 100
        }
        N = rns.size();
        Collections.sort(rns);
        
        double dp = dPlus();
        double dm = dMinus();
        Dtest = Math.max(dp, dm);
        return Dtest;
        
    }
    public boolean getResult(sigLevel significance)
    {
        double sigVal[] = {1.07,1.22,1.36};  //From: http://www.cas.usf.edu/~cconnor/colima/Kolmogorov_Smirnov.htm
        double da = sigVal[significance.ordinal()] / Math.sqrt(N); //Assumes over N > 35
        return Dtest < da; 
    }
    private double dPlus()
    {
        double max = -1;
     
        for(int i = 0; i < N; i++)
        {
            double v = ((double)i/(double)N) - rns.get(i).getValue();
            if(v > max)
            {
                max = v;
            }
        }
        return max;
        
    }
    private double dMinus()
    {
        double max = -1;
      
        for(int i = 0; i < N; i++)
        {
            double v = rns.get(i).getValue() - ((double)(i-1)/(double)N);
            if(v > max)
            {
                max = v;
            }
        }
        return max;
    }

}
